package com.wipro.set.gateway.address.api.model;

import lombok.Data;

@Data
public class Address {

    private String street;
    private String city;
    private String state;
    private String neighborhood;
    private String postalCode;
}
