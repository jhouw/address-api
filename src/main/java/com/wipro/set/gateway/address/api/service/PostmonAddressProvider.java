package com.wipro.set.gateway.address.api.service;

import com.wipro.set.gateway.address.api.model.Address;
import com.wipro.set.gateway.address.api.model.AddressProvider;
import lombok.extern.log4j.Log4j2;
import org.dozer.Mapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;
import org.springframework.web.client.HttpClientErrorException;
import org.springframework.web.client.RestTemplate;

@Component
@Log4j2
public class PostmonAddressProvider implements AddressProvider {

    @Value("${address.provider.url}")
    private String url;

    @Value("${address.provider.error-msg-template}")
    private String errorMsgTemplate;

    private final Mapper mapper;

    @Autowired
    public PostmonAddressProvider(Mapper mapper) {
        this.mapper = mapper;
    }

    @Override
    public Address getAddress(String postalCode) {
        RestTemplate template = new RestTemplate();

        try {
            PostmonAddress address = template.getForObject(
                    url.concat(postalCode), PostmonAddress.class);

            return mapper.map(address, Address.class);

        } catch (HttpClientErrorException e){
            log.error(e);
            String msg = String.format(errorMsgTemplate, postalCode);
            throw new InvalidPostalCodeException(msg);
        }
    }
}
