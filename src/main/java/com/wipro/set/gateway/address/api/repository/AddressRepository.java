package com.wipro.set.gateway.address.api.repository;

import com.wipro.set.gateway.address.api.model.Address;
import org.springframework.data.mongodb.repository.MongoRepository;

public interface AddressRepository extends MongoRepository<Address, String> {
}
