package com.wipro.set.gateway.address.api.model;

public interface AddressProvider {
    Address getAddress(String postalCode);
}
