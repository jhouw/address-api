package com.wipro.set.gateway.address.api.controller;

import com.wipro.set.gateway.address.api.model.Address;
import com.wipro.set.gateway.address.api.model.AddressProvider;
import com.wipro.set.gateway.address.api.repository.AddressRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
public class AddressController {

    private final AddressProvider addressProvider;

    private final AddressRepository repository;

    @Autowired
    public AddressController(AddressProvider addressProvider,
                             AddressRepository repository) {
        this.addressProvider = addressProvider;
        this.repository = repository;
    }

    @GetMapping("/address/{postalCode}")
    public ResponseEntity<?> getAddress(@PathVariable String postalCode){
        Address address = addressProvider.getAddress(postalCode);
        return ResponseEntity.ok(address);
    }

    @GetMapping("/address/all")
    public ResponseEntity<?> listAddress(){
        return ResponseEntity.ok(repository.findAll());
    }

    @PostMapping("/address")
    public ResponseEntity<?> saveAddress(@RequestBody Address address){
        repository.save(address);
        return ResponseEntity.ok(address.getPostalCode());
    }
}
