package com.wipro.set.gateway.address.api.service;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;

@Data
@JsonIgnoreProperties(ignoreUnknown = true)
public class PostmonAddress {

    @JsonProperty("logradouro")
    private String street;

    @JsonProperty("cidade")
    private String city;

    @JsonProperty("estado")
    private String state;

    @JsonProperty("bairro")
    private String neighborhood;

    @JsonProperty("cep")
    private String postalCode;
}
